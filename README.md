Main URLS
- https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.17/
- https://www.haproxy.com/documentation/hapee/1-9r1/traffic-management/kubernetes-ingress-controller/
- kubectl apply -f https://raw.githubusercontent.com/haproxytech/kubernetes-ingress/master/deploy/haproxy-ingress.yaml
- https://haproxy-ingress.github.io/docs/examples/blue-green/
- https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/
- https://github.com/kubernetes/examples/blob/master/staging/simple-nginx.md
- https://cloud.google.com/kubernetes-engine/docs/concepts/deployment
- https://kubernetes.io/docs/concepts/workloads/controllers/deployment/


Commands
- https://kubernetes.io/docs/reference/kubectl/cheatsheet/
- kubectl get ing
- kubectl get svc -A
- kubectl get ep
- kubectl get pod
- kubectl get deploy
- kubectl describe ...
- kubectl get pods --show-labels
- kubectl api-resources --verbs=list -o name | xargs -n 1 kubectl get -o name
- kubectl describe pods blue-584b5b7d78-vcbct | grep Node
- kubectl get -o=yaml ing busknginx

Load Testing
```
$ IP=10.x.x.x
$ alias hareq='echo Running 100 requests...; for i in `seq 1 100`; do
    curl -fsS $IP -H "Host: bluegreen.example.com" | cut -d- -f1
  done | sort | uniq -c'
```

Tracing
```
conntrack -L
To watch continuously for new connections, use the -E flag:

conntrack -E
To list conntrack-tracked connections to a particular destination address, use the -d flag:

conntrack -L -d 10.32.0.1
```

More

- https://www.haproxy.com/blog/dissecting-the-haproxy-kubernetes-ingress-controller/
- https://kubernetes.io/docs/concepts/services-networking/network-policies/


Very Granular
- http://dockerlabs.collabnix.com/kubernetes/beginners/Install-and-configure-a-multi-master-Kubernetes-cluster-with-kubeadm.html


Gitlab Kubernetes

- https://docs.gitlab.com/charts/advanced/persistent-volumes/
- https://gitlab.com/gitlab-examples/kubernetes-deploy